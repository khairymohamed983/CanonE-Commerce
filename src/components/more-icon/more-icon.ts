import { Component, Input } from '@angular/core';
import { MorePopverPage } from '../../pages/more-popver/more-popver';
import { PopoverController } from 'ionic-angular';

@Component({
  selector: 'more-icon',
  templateUrl: 'more-icon.html'
})
export class MoreIconComponent {

  @Input('inputPageName') inputPageName;
  pageName;
  popvarSpecialsArr = ['Home','My Account','Cart','Wishlist','My Orders'];
  popvarCartArr = ['Home','My Account','Wishlist','My Orders'];
  popvarWishlistArr = ['Home','My Account','Cart','Specials','My Orders'];
  popvarOrderArr = ['Home','My Account','Cart','Wishlist','Specials'];

  constructor(public popoverCtrl: PopoverController) {
    this.pageName = this.inputPageName
  }

  presentPopoverSpecial(myEvent) {
    let popover = this.popoverCtrl.create(MorePopverPage, {popvarArr: this.popvarSpecialsArr});
    console.log('my event', myEvent);
    popover.present({
      ev: myEvent
    });

  }

  presentPopoverCart(myEvent) {
    let popover = this.popoverCtrl.create(MorePopverPage, {popvarArr: this.popvarCartArr});
    popover.present({
      ev: myEvent
    });
  }

  presentPopoverWishlist(myEvent) {
    let popover = this.popoverCtrl.create(MorePopverPage, {popvarArr: this.popvarWishlistArr});
    popover.present({
      ev: myEvent
    });
  }

  presentPopoverOrder(myEvent) {
    let popover = this.popoverCtrl.create(MorePopverPage, {popvarArr: this.popvarOrderArr});
    popover.present({
      ev: myEvent
    });
  }

}
