import { CartPage } from '../../pages/cart/cart';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'cart-icon',
  templateUrl: 'cart-icon.html'
})
export class CartIconnComponent {

  text: string;

  constructor(public navCtr:NavController) {
    console.log('Hello CartIconComponent Component');
    this.text = 'Hello World';
  }

  goToShoopingCart() {
    this.navCtr.setRoot(CartPage);
  }

}
