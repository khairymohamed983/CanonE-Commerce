import { debounce } from 'ionic-angular/util/util';
import { Injectable } from '@angular/core';
import { HttpProvider } from '../http/http';
import { URLSearchParams } from '@angular/http';
import { HTTP } from '@ionic-native/http';
import { writeToNodes } from 'ionic-angular/components/virtual-scroll/virtual-util';

// import { Storage } from '@ionic/storage';


@Injectable()
export class CartProvider {

  headers: any;

  access_token: any;

  constructor(public http: HTTP) {
    this.headers = { "Authorize":  window.localStorage.getItem('access-token') };
  }


  changeQuantity(cartid, quantity) {
    let body = { "id": cartid, "quantity": quantity };
    return this.http.post('http://alsayershop.com/public/api/update_cart', body, this.headers).then(res => {
      return JSON.parse(res.data).data;
    });
  }

  getMyCart() {
    return this.http.get('http://alsayershop.com/public/api/show_cart', {},
     {"Authorize":window.localStorage.getItem("access-token")}).then(res => {
      return JSON.parse(res.data).data;
    },err=>{
      alert("err"+err)
    });
  }

  addToCart(id) {
    let body = {'product_id': id};
    return this.http.post('http://alsayershop.com/public/api/add_cart', body, this.headers).then(res => {
      return JSON.parse(res.data).data;
    });
  }

  removeFromCart(cartid) {
    let body = { 'cart_id': cartid };

    return this.http.post('http://alsayershop.com/public/api/remove_cart', body, this.headers)
      .then(res => {
        return this.getMyCart();
      });
  }

  clearCart() {
    return this.http.post('http://alsayershop.com/public/api/clear_cart', {}, this.headers)
      .then(res => {
        return JSON.parse(res.data).data;
      });
  }
}
