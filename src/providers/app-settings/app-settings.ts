import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

@Injectable()
export class AppSettingsProvider {

    language;

    constructor(public storage:Storage,) {
        this.getCurrentLanguage();
        console.log('language from sevice', this.language);
    }

    getCurrentLanguage() {
        this.storage.get('lang').then((val)=>{
            if(val==null) {
                this.language = 'en';
            } else {
                this.language = val;
            }
        }) 
    }

    get Language() {
        return this.language;
    }

}
