import { Injectable } from '@angular/core';
import { HttpProvider } from '../http/http';
import {  URLSearchParams } from '@angular/http';
// import { Storage } from '@ionic/storage';
import { HTTP } from '../../../node_modules/@ionic-native/http';


@Injectable()
export class ProductProvider {

  // options: RequestOptions;
  
  constructor(public HTTP: HTTP) {
   
  }

  getProducts(type, price, rating, name, page) {
    var url = 'products?type=' +  type  + '&price=' + price + '&rating=' +  rating + '&name=' +  name + '&page=' + page;
    return this.HTTP.get(url,{},{})
      .then(res => {
        return JSON.parse(res.data).data;
      });
  }

  getCategoryProducts(id) {
    return this.HTTP.get('http://alsayershop.com/public/api/category/' + id,{}, {'Accept-Language':'ar'} )
      .then(res => {
        return JSON.parse(res.data).data;
      }, err => {
        console.log("error getCategoryProducts ");
      });
  }

  
  addToFavourite(obj) {
    if (obj.in_wishlist) { // remove from with list
      obj.in_wishlist = false;
      let body = new URLSearchParams();
      body.set('wishlist_id', obj.wishlist_id);
      return this.HTTP.post('remove_wishlist', body,{}).then(res => {
        debugger;
        if (JSON.parse(res.data).status == 200) {
          obj.wishlist_id = '';
          return obj;
        }
      });
    } else {  // add to wish list
      obj.in_wishlist = true;
      debugger;
      let body = new URLSearchParams();
      body.set('product_id', obj.id);
      return this.HTTP.post('add_wishlist', body,{}).then(res => {
        
        if (JSON.parse(res.data).status == 200) {
          obj.wishlist_id = JSON.parse(res.data).data.Wishlist_id;
          return obj;
        }
      });
    }
  }

  removeFromWishlist(id) {
    let body = new URLSearchParams();
    body.set('wishlist_id', id);
    return this.HTTP.post('remove_wishlist', body,{}).then(res => {
      return JSON.parse(res.data).data;
    });
  }

  getMyWishList() {
    return this.HTTP.get('show_wishlist',{},{}).then(res => {
      return JSON.parse(res.data).data;
    });
  }

  addReview(addReviewModel) {
    console.log(addReviewModel);
    let body = new URLSearchParams();
    body.set('product_id', addReviewModel.productId );
    body.set('review', addReviewModel.review);
    body.set('rating', addReviewModel.rating );
    
    return this.HTTP.post('add_review', body,{})
      .then(res => {
        return JSON.parse(res.data).data;
      }, err => {
        debugger;
      });
  }

  getProduct(id) {
    return this.HTTP.get('http://alsayershop.com/public/api/product/' + id,{},{'Accept-Language':'ar'}).then(res => {
      return JSON.parse(res.data).data;
    });
  }
}
