import { Injectable } from '@angular/core';
import { HttpProvider } from '../http/http';
import { URLSearchParams } from '@angular/http';

import { HTTP } from '../../../node_modules/@ionic-native/http';






@Injectable()
export class HomeProvider {



  constructor(public http: HttpProvider, public HTTP: HTTP) {
    // this.storage.get('access_token').then((val) => {
    //   let headers = new Headers({ 'Accept': 'application/json', 'Authorize': val });
    //   this.options = new RequestOptions({ headers: headers });
    // });


  }

  getMainSlider() {
    return this.HTTP.get('http://alsayershop.com/public/api/sliders', {}, {})
      .then((res) => {
        return JSON.parse(res.data).data;
      })
      .catch((error) => {
        console.log("prov error:" + error);
      });
  }

  getBrands() {

    return this.HTTP.get("http://alsayershop.com/public/api/brands", {}, {}).then(res => {
      return JSON.parse(res.data).data;
    });
  }

  getProducts(access_token) {

    let headers = {"Accept-Language": window.localStorage.getItem('language')};
    return this.HTTP.get("http://alsayershop.com/public/api/productgroups", {}, headers).then(res => {
      let ress = JSON.parse(res.data);
      
      return {
        latestProducts: ress.latest_products,
        specialArr: ress.special_products,
        bestseallers: ress.most_sells
      }
    });
  }



  sendMsg(model) {
    let body = new URLSearchParams();
    body.set('subject', 'qq');
    body.set('name', model.name);
    body.set('email', model.email);
    body.set('message', model.message);

    return this.http.post('send_contact', body).map(res => {
      return res.json();
    }, err => {
      debugger;
    });

  }




}
