
import { Injectable } from '@angular/core';
import { Http, XHRBackend, RequestOptions, Request, RequestOptionsArgs, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
const baseUrl = 'http://alsayershop.com/public/api/';

@Injectable()
export class HttpProvider extends Http {

  constructor(backend: XHRBackend, options: RequestOptions) {
    super(backend, options);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
   
    
    if (!options) {
      options = { headers: new Headers() };
    }
    var access_token = window.localStorage.getItem('access_tooken');
    var language = window.localStorage.getItem('language');
 
    if (typeof url === 'string') {
      options.headers.set('Authorize', access_token);
      options.headers.set('Accept', 'application/json');
      options.headers.set('Accept-Language', language);
      url = baseUrl + url;
    } else {
      
      url.headers.append('Authorize', access_token);
      url.headers.append("Accept", "application/json");
      url.headers.append("Accept-Language", language);
      url.url = baseUrl + url.url;
    }
    return super.request(url, options).catch(this.catchAuthError(this));
  }

  private catchAuthError(self: HttpProvider) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      if (res.status === 401 || res.status === 403) {
        // if not authenticated
        // redirect user to login page
        // this.router.navigate(['/auth/login']);
        console.log(res);
      }
      return Observable.throw(res);
    };
  }
}
