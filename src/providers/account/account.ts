import { Injectable } from '@angular/core';
import { HttpProvider } from '../http/http';
// import { Storage } from '@ionic/storage';
 import { URLSearchParams } from '@angular/http';

@Injectable()
export class AccountProvider {
  // options: RequestOptions;

  constructor(public http: HttpProvider ) {
    // this.storage.get('access_token').then((val) => {
    //   let headers = new Headers({ 'Accept': 'application/json', 'Authorize': val });
    //   this.options = new RequestOptions({ headers: headers });
    // });
  }

  getUserData() {
    return this.http.get('user').map(res => {
      return res.json().data;
    });
  }

  updateUserData(userData, base64Image) {
    let body = new URLSearchParams();
    body.append('name', userData.name);
    body.append('mobile', userData.mobile);
    body.set('email', userData.email);
    body.set('gender', userData.gender);
    if (base64Image != '')
      body.set('image', base64Image);

    return this.http.post('update_user', body)
      .map(res => {
        return res.json();
      });
  }

  deleteAccount() {
    return this.http.post('delete_user', {})
      .map(res => {
        return res.json();
        // logout  then goto home 
      });
  }
}
