import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Config, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions, Headers } from '@angular/http';


import { HomePage } from '../pages/home/home';
import { SpecialsPage } from "../pages/specials/specials";
import { CartPage } from "../pages/cart/cart";
import { WishlistPage } from "../pages/wishlist/wishlist";
import { MyOrdersPage } from "../pages/my-orders/my-orders";
import { CategoriesPage } from "../pages/categories/categories";
import { MyAccountPage } from '../pages/my-account/my-account';
import { SettingsPage } from '../pages/settings/settings';
import { ContactUsPage } from "../pages/contact-us/contact-us";
import { AboutUsPage } from "../pages/about-us/about-us";

import { AppSettingsProvider } from '../providers/app-settings/app-settings';

export interface MenuItem {
  title: string;
  component: any;
  iosIcon: string;
  mdIcon: string;
  mdIconSelected: string;
  isDisabled: boolean;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
  appMenuItems: Array<MenuItem>;
  accountMenuItems: Array<MenuItem>;


  accessToken: string;


  userData: any = {
    name: "",
    email: "",
    image: "http://alsayershop.com/public/uploads/profile/avatar.png"
  };


  public isLogedId: boolean;
  public mode: string;
  public modeText: string;
  activPage: any;
  language;
  menuDirection: string = 'right';

  constructor(public appSettingsProvider: AppSettingsProvider, public events: Events,
    public translateService: TranslateService, public storage: Storage,
    public platform: Platform, public statusBar: StatusBar,
    config: Config, public splashScreen: SplashScreen, public http: Http) {

    this.mode = config.get('mode');
    if (this.mode == 'ios') {
      this.modeText = 'Rate Us on Apple Store';
    } else if (this.mode == 'md') {
      this.modeText = 'Rate Us on Google Play';
    }
    // let status bar overlay webview
    // this.statusBar.overlaysWebView(true);
    this.statusBar.styleDefault();
    // set status bar to white
    // this.statusBar.backgroundColorByHexString('#ffffff');
    // this.getStorageElements();
    this.initializeApp();
    this.getLAnguageFromEvents();



    this.storage.get('hasLoggedIn').then((val) => {
      this.isLogedId = val;
      this.setSideMenu();
    });


    this.storage.get('access_token').then((val) => {
      window.localStorage.setItem('access_tooken', val);
      this.accessToken = val;
      if (val != '')
        this.getUserData();
    });

  }


  getUserData() {

    let headers = new Headers({ 'Accept': 'application/json', 'Authorize': this.accessToken });
    let options = new RequestOptions({ headers: headers });
    this.http.get('http://alsayershop.com/public/api/user', options).subscribe(res => {
      this.userData = res.json().data;
    });
  }

  setSideMenu() {
    window.localStorage.setItem('language', "ar");


    this.translateService.get(['SPECIALS', 'HOME', 'SHOPPINGCART', 'WISHLIST', 'MYORDERS',
      'CATEGORIES', 'GIFTVOUCHERS', 'AFFILIATES', 'RETURNS', 'MYACCOUNT', 'SETTINGS',
      'SERVICECENTER', 'CONTACTUS', 'ABOUT', 'LOGOUT', 'LOGIN'])
      .subscribe(translations => {
        console.log(translations);
        this.appMenuItems = [
          { title: translations.SPECIALS, component: SpecialsPage, iosIcon: './assets/icons/iosmenu/specials.svg', mdIcon: './assets/icons/mdmenu/specials.svg', mdIconSelected: './assets/icons/mdmenu/specials.svg', isDisabled: false },
          { title: translations.HOME, component: HomePage, iosIcon: './assets/icons/iosmenu/home.svg', mdIcon: './assets/icons/mdmenu/home.svg', mdIconSelected: './assets/icons/mdmenu/home_selected.svg', isDisabled: false },
          { title: translations.CATEGORIES, component: CategoriesPage, iosIcon: './assets/icons/iosmenu/categories.svg', mdIcon: './assets/icons/mdmenu/categories.svg', mdIconSelected: './assets/icons/mdmenu/categories_selected.svg', isDisabled: false },
          // { title: translations.GIFTVOUCHERS, component: CategoriesPage, iosIcon: './assets/icons/iosmenu/giftvouchers.svg', mdIcon: './assets/icons/mdmenu/giftvouchers.svg', mdIconSelected: './assets/icons/mdmenu/giftvouchers_selected.svg', isDisabled: true },
          // { title: translations.AFFILIATES, component: CategoriesPage, iosIcon: './assets/icons/iosmenu/affiliates.svg', mdIcon: './assets/icons/mdmenu/affiliates.svg', mdIconSelected: './assets/icons/mdmenu/affiliates_selected.svg', isDisabled: true },
          // { title: translations.SERVICECENTER, component: CategoriesPage, iosIcon: './assets/icons/iosmenu/servicecenter.svg', mdIcon: './assets/icons/mdmenu/servicecenter.svg', mdIconSelected: './assets/icons/mdmenu/servicecenter_selected.svg', isDisabled: true },
          { title: translations.CONTACTUS, component: ContactUsPage, iosIcon: './assets/icons/iosmenu/contact.svg', mdIcon: './assets/icons/mdmenu/contact.svg', mdIconSelected: './assets/icons/mdmenu/contact_selected.svg', isDisabled: false },
          { title: translations.ABOUT, component: AboutUsPage, iosIcon: './assets/icons/iosmenu/about.svg', mdIcon: './assets/icons/mdmenu/about.svg', mdIconSelected: './assets/icons/mdmenu/about_selected.svg', isDisabled: false },
          // { title: this.modeText, component: CategoriesPage, iosIcon: './assets/icons/iosmenu/rate.svg', mdIcon: './assets/icons/mdmenu/rate.svg', mdIconSelected: './assets/icons/mdmenu/rate_selected.svg', isDisabled: true },
          { title: this.isLogedId ? translations.LOGOUT : translations.LOGIN, component: LoginPage, iosIcon: './assets/icons/iosmenu/logout.svg', mdIcon: './assets/icons/mdmenu/logout.svg', mdIconSelected: './assets/icons/mdmenu/logout_selected.svg', isDisabled: false }
        ];

        if (this.isLogedId) {
          this.accountMenuItems = [
            { title: translations.MYACCOUNT, component: MyAccountPage, iosIcon: './assets/icons/iosmenu/account.svg', mdIcon: './assets/icons/mdmenu/account.svg', mdIconSelected: './assets/icons/mdmenu/account_selected.svg', isDisabled: false },
            { title: translations.SHOPPINGCART, component: CartPage, iosIcon: './assets/icons/iosmenu/cart.svg', mdIcon: './assets/icons/mdmenu/cart.svg', mdIconSelected: './assets/icons/mdmenu/cart_selected.svg', isDisabled: false },
            { title: translations.WISHLIST, component: WishlistPage, iosIcon: './assets/icons/iosmenu/wishlist.svg', mdIcon: './assets/icons/mdmenu/wishlist.svg', mdIconSelected: './assets/icons/mdmenu/wishlist_selected.svg', isDisabled: false },
            { title: translations.MYORDERS, component: MyOrdersPage, iosIcon: './assets/icons/iosmenu/orders.svg', mdIcon: './assets/icons/mdmenu/orders.svg', mdIconSelected: './assets/icons/mdmenu/orders_selected.svg', isDisabled: false },
            { title: translations.SETTINGS, component: SettingsPage, iosIcon: './assets/icons/iosmenu/settings.svg', mdIcon: './assets/icons/mdmenu/settings.svg', mdIconSelected: './assets/icons/mdmenu/settings_selected.svg', isDisabled: false },
            // { title: translations.RETURNS, component: CategoriesPage, iosIcon: './assets/icons/iosmenu/returns.svg', mdIcon: './assets/icons/mdmenu/returns.svg', mdIconSelected: './assets/icons/mdmenu/returns_selected.svg', isDisabled: false }
          ]
        } else {
          this.accountMenuItems = [];
        }
      })

  }


  getLAnguageFromEvents() {
    this.events.subscribe('lang', (val) => {
      if (val == null) {
        this.translateService.use('ar');
        this.language = "ar";
        
        // this.menuDirection = "left";
      } else {
        this.translateService.use(val);
        this.language = val;
        this.setSideMenu();
        if (val == 'ar') {
          this.platform.setDir('rtl', true);
          // this.menuDirection = "right";
        } else if (val == 'en') {
          this.platform.setDir('ltr', true);
          // this.menuDirection = "left";
        }
      }

      window.localStorage.setItem('language', this.language);
    });
  }

  getDir() {
    return (this.menuDirection);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();

      this.language = "ar";
      this.translateService.use('ar');
      this.platform.setDir('rtl', true);
      this.setSideMenu();
      this.splashScreen.hide();

    });
  }

  openPage(page, title) {
    // logout
    if (page.component.name == "LoginPage")
      this.logout();

    // spcial 
    else if (title == "SPECIALS" || title == "العروض الخاصة")
      this.nav.push(page.component, {
        type: 'special'
      });
    else if (page.component.name == "HomePage")
      this.nav.setRoot(HomePage);
    else
      this.nav.push(page.component);

    this.activPage = title;
  }

  openLoginPage() {
    this.nav.push(LoginPage, {
      'from': 'root'
    })
  }

  logout() {
    if (this.isLogedId) {
      window.localStorage.setItem('access-token', '');
      this.storage.set('hasLoggedIn', false);
      this.storage.set('access_token', '');
      this.nav.setRoot(HomePage);
      this.isLogedId = false;
      this.setSideMenu();
    } else {
      this.nav.push(LoginPage);
    }
  }
}
