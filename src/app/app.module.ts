import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Ionic2RatingModule } from 'ionic2-rating';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Camera } from '@ionic-native/camera';

import { HttpModule, Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

// import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule } from '@ionic/storage';

//Components
import { CartIconnComponent } from '../components/cart-icon/cart-icon';
import { MoreIconComponent } from '../components/more-icon/more-icon';

//pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SpecialsPage } from "../pages/specials/specials";
import { SearchPage } from "../pages/search/search";
import { CartPage } from "../pages/cart/cart";
import { ProductPage } from "../pages/product/product";
import { DescriptionPage } from "../pages/description/description";
import { ReviewPage } from "../pages/review/review";
import { CheckoutPage } from "../pages/checkout/checkout";
import { DeliveryDatailsPage } from "../pages/delivery-datails/delivery-datails";
import { CashDeliveryPage } from "../pages/cash-delivery/cash-delivery";
import { CreditCardPage } from "../pages/credit-card/credit-card";
import { WishlistPage } from "../pages/wishlist/wishlist";
import { MyOrdersPage } from "../pages/my-orders/my-orders";
import { CategoriesPage } from "../pages/categories/categories";
import { MyAccountPage } from "../pages/my-account/my-account";
import { EditAccountPage } from "../pages/edit-account/edit-account";
import { SettingsPage } from "../pages/settings/settings";
import { ContactUsPage } from "../pages/contact-us/contact-us";
import { SendMessagePage } from "../pages/send-message/send-message";
import { AboutUsPage } from "../pages/about-us/about-us";
import { LoginWithEmailPage } from '../pages/login-with-email/login-with-email';
import { LoginPage } from '../pages/login/login';
import { LanguagePopverPage } from '../pages/language-popver/language-popver';
import { MorePopverPage } from '../pages/more-popver/more-popver';
import { AddReviewPopoverPage } from '../pages/add-review-popover/add-review-popover';
import { RegisterPage } from '../pages/register/register';
import { ShareOptionsPage } from '../pages/share-options/share-options';
import { SortPopoverPage } from '../pages/sort-popover/sort-popover';
import { SearchPopoverPage } from '../pages/search-popover/search-popover';
import { AppSettingsProvider } from '../providers/app-settings/app-settings';
import { HttpProvider } from '../providers/http/http';
import { AuthProvider } from '../providers/auth/auth';
import { HomeProvider } from '../providers/home/home';
import { ProductProvider } from '../providers/product/product';
import { CartProvider } from '../providers/cart/cart';
import { AccountProvider } from '../providers/account/account';
import { Facebook } from '@ionic-native/facebook';


import { TranslateService } from '@ngx-translate/core';
import { HTTP } from '@ionic-native/http';


export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    CartIconnComponent,
    MoreIconComponent,
    HomePage,
    SpecialsPage,
    SearchPage,
    CartPage,
    ProductPage,
    DescriptionPage,
    ReviewPage,
    CheckoutPage,
    DeliveryDatailsPage,
    CashDeliveryPage,
    CreditCardPage,
    WishlistPage,
    MyOrdersPage,
    CategoriesPage,
    MyAccountPage,
    EditAccountPage,
    SettingsPage,
    ContactUsPage,
    SendMessagePage,
    AboutUsPage,
    LoginWithEmailPage,
    LoginPage,
    LanguagePopverPage,
    MorePopverPage,
    AddReviewPopoverPage,
    RegisterPage,
    ShareOptionsPage,
    SortPopoverPage,
    SearchPopoverPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    Ionic2RatingModule,
    IonicStorageModule.forRoot(),
    HttpModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    })
  ],
  exports: [
    TranslateModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SpecialsPage,
    SearchPage,
    CartPage,
    ProductPage,
    DescriptionPage,
    ReviewPage,
    CheckoutPage,
    DeliveryDatailsPage,
    CashDeliveryPage,
    CreditCardPage,
    WishlistPage,
    MyOrdersPage,
    CategoriesPage,
    MyAccountPage,
    EditAccountPage,
    SettingsPage,
    ContactUsPage,
    SendMessagePage,
    AboutUsPage,
    LoginWithEmailPage,
    LoginPage,
    LanguagePopverPage,
    MorePopverPage,
    AddReviewPopoverPage,
    RegisterPage,
    ShareOptionsPage,
    SortPopoverPage,
    SearchPopoverPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SocialSharing,
    Camera,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AppSettingsProvider,
    HttpProvider,
    AuthProvider,
    HomeProvider,
    ProductProvider,
    CartProvider,
    AccountProvider,
    TranslateService,
    Facebook,
    HTTP
  ]
})
export class AppModule { }
