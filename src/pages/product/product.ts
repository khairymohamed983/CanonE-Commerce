import { CartPage } from './../cart/cart';
import { ReviewPage } from '../review/review';
import { DescriptionPage } from '../description/description';
import { Component } from '@angular/core';
import { ModalController, NavController, NavParams, PopoverController, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';


@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})

export class ProductPage {
  popvarOrderArr = ['Home', 'My Account', 'Cart', 'Wishlist', 'Specials'];
  productsArr: number[] = new Array(6);
  reviewsArr: number[] = new Array(2);
  hashtagSliderArr: number[] = new Array(6);
  contactModal;
  firstViewCtrl;
  rate = 3;
  productId: number;

  currentProduct: any = {};

  hasLoggedIn: boolean;

  lstReviews = [];
  lstRelatedProducts = [];
  lstImages = [];
  lstCategories = [{ "id": 12, "name": '1212' }];

  constructor(public appSettingsProvider: AppSettingsProvider,
    public modalCtrl: ModalController, public navCtrl: NavController,
    public navParams: NavParams, public popoverCtrl: PopoverController,
    public viewCtr: ViewController, private socialSharing: SocialSharing,
    public productSrvc: ProductProvider,
    public cartSrvc: CartProvider,
    public http: Http, public storage: Storage, private toastCtrl: ToastController, public loadingCtrl: LoadingController) {

    // console.log(this.getCurrentLAnguage());
    this.productId = this.navParams.get("id");
    this.getCurrentProduct(this.productId);

    this.storage.get('hasLoggedIn').then((val) => {
      this.hasLoggedIn = val;
    });
  }

  // ngAfterViewInit() {
  //   this.getCurrentProduct(this.productId);
  // }

  ionViewDidLoad() {
    this.getCurrentProduct(this.productId);
  }

  getCurrentProduct(id) {//this.currentProduct.in_wishlist=true;
    let loading = this.loadingCtrl.create({ content: '' });
    loading.present();

    this.productSrvc.getProduct(id).then(data => {

      this.currentProduct = data;
      this.lstCategories = data.main_category;
      this.lstImages = data.photos.data;

      if (data.relatedproducts)
        this.lstRelatedProducts = data.relatedproducts.data;
      if (data.reviews)
        this.lstReviews = data.reviews.data;

        
      loading.dismiss();
    }, err => {
      console.log(err);
    });
  }

  starClicked(value) {
    console.log("Avaliaram em :", value);
  }

  gotoDecriptionPage(desc) {
    this.navCtrl.push(DescriptionPage, { desc: desc });
  }

  goToReviewsPage(productId, lst) {
    this.navCtrl.push(ReviewPage, { productId: productId, lstReviews: lst });
  }

  goToProductPage() {
    this.navCtrl.push(ProductPage);
  }

  share() {
    this.socialSharing.share('Hello World', 'Attention', null, 'https://www.thepolyglotdeveloper.com').then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }

  addCart(id) {
    if (this.hasLoggedIn) { // check if has logged in 
      this.cartSrvc.addToCart(id).then(data => {
        if (data.status == '200') { // success
          let msg = this.getCurrentLAnguage() == 'en' ? 'Added Successfuly.' : 'تمت الاضافة بنجاح';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        } else if (data.status == '400') { // added before
          let msg = this.getCurrentLAnguage() == 'en' ? 'This Product Added Before.' : 'عفوا هذا المنتج مضاف الى السلة من قبل';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        }
      }, err => {
        let msg = this.getCurrentLAnguage() == 'en' ? 'This Product Added Before.' : 'عفوا هذا المنتج مضاف الى السلة من قبل';
        let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
        toast.present();
      });
    } else {
      let msg = this.getCurrentLAnguage() == 'en' ? 'You must login first.' : 'سجل دخول اولا';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }
  }


  gotocart(id) {
    if (this.hasLoggedIn) {
    this.addCart(id);
    this.navCtrl.push(CartPage);
    }else{
      let msg = this.getCurrentLAnguage() == 'en' ? 'You must login first.' : 'سجل دخول اولا';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }
  }

  addToWishlist(obj) {
    // check if logged in first
    if (this.hasLoggedIn) {
      this.productSrvc.addToFavourite(obj).then(data => {
        obj = data;
      }, err => {
        console.log(err);
      });
    } else {
      let msg = this.getCurrentLAnguage() == 'en' ? 'You must login first.' : 'سجل دخول اولا';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }
  }

  getCurrentLAnguage() {
    return this.appSettingsProvider.Language;
  }


}
