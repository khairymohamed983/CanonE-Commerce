import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginWithEmailPage } from '../login-with-email/login-with-email';
import { RegisterPage } from '../register/register';
import { HomePage } from '../home/home';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { AuthProvider } from '../../providers/auth/auth'
import { MyApp } from './../../app/app.component';
import { Storage } from '@ionic/storage';
import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  status;
  signinData = {
    username: "",
    client_id: "2",
    grant_type: 'password',
    scope: '*',
    client_secret: 'VpkTtQe71mg2Cioua0Igr4KVUzeCGLNGsNMX2KQA'
  };

  constructor(public appSettingsProvider: AppSettingsProvider, public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthProvider
    , public myApp: MyApp, private alertCtrl: AlertController,
    private storage: Storage,
    public http: Http, private fb: Facebook
  ) {
    this.status = this.navParams.get('status');
    console.log('langusge', this.appSettingsProvider.Language);
  }

  loginWithEmail() {
    this.navCtrl.push(LoginWithEmailPage);
  }

  openRegisterPage() {
    this.navCtrl.push(RegisterPage);
  }

  backToHome() {
    this.navCtrl.setRoot(HomePage);

    // if (!this.navCtrl.canGoBack()) {
    //   this.navCtrl.setRoot(HomePage);
    // } else {
    //   this.navCtrl.pop();
    // }
  }

  getCurrentLAnguage() {
    return this.appSettingsProvider.Language;
  }

  loginWithTwiter() {
    // this.googlePlus.login({})
    //   .then(res => alert('loged in successfully' + res))
    //   .catch(err => alert('error loging in ' + err));
  }


  loginWithFacebook() {
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((fbres: FacebookLoginResponse) => {
        console.log(fbres);

        let body = new URLSearchParams();
        body.set('client_id', this.signinData.client_id);
        body.set('client_secret', this.signinData.client_secret);
        body.set('grant_type', this.signinData.grant_type);
        body.set('scope', this.signinData.scope);
        body.set('provider', 'facebook');
        body.set('provider_user_id', fbres.authResponse.userID);
        body.set('scope', this.signinData.scope);
        body.set('username', fbres.authResponse.userID);
        body.set('mobile', '');
        body.set('email', '');
        body.set('name', '');
        this.http.post('http://alsayershop.com/public/api/login/social', body.toString(), {
          headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Accept-Language': 'en',
          })
        }).subscribe(res => {
          this.storage.set('hasLoggedIn', true);
          this.storage.set('access_token', res.json().data.access_token);
          localStorage.setItem('access_tooken', res.json().data.access_token);
          this.navCtrl.setRoot(HomePage);
          this.myApp.isLogedId = true;
          this.myApp.setSideMenu();
        }, err => {
          let btnName = this.getCurrentLAnguage() == 'ar' ? 'تم' : 'ok';
          let alert = this.alertCtrl.create({ title: '', subTitle: err.json().message, buttons: [btnName] });
          alert.present();
        });
      }).catch((err) => {
        alert(err)
      });
  }
}
