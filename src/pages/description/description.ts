import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';




@Component({
  selector: 'page-description',
  templateUrl: 'description.html',
})
export class DescriptionPage {

description:string;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.description = this.navParams.get("desc");
  }

  ionViewDidLoad() {

   // console.log('ionViewDidLoad DescriptionPage');
  }

}
