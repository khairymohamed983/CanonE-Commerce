import { LoginPage } from './../login/login';
// import { debounce } from 'ionic-angular/util/util';
import { AppSettingsProvider } from './../../providers/app-settings/app-settings';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { CheckoutPage } from "../checkout/checkout";
import { CartProvider } from '../../providers/cart/cart';
import { ProductProvider } from '../../providers/product/product';




// increaseQuantity

@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {

  productQuantity: number = 1;
  productArr = [];
  hasLoggedIn: boolean;
  accessToken: string;
  emptyMsg: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, public cartSrvc: CartProvider,
    public productSrvc: ProductProvider, public appSetting: AppSettingsProvider,
    public loadingCtrl: LoadingController, public appSettingsProvider: AppSettingsProvider, public toastCtrl: ToastController) {

    this.storage.get('hasLoggedIn').then((val) => { this.hasLoggedIn = val; });
    this.emptyMsg = this.appSetting.language == 'ar' ? 'عفوا لا توجد منتجات مضافة الى القائمة المفضلة لديك' :
     'Sorry  their is no products added to your withlist ';





  }

  ionViewWillEnter() {

  }

  ionViewCanEnter() {
    let token=window.localStorage.getItem("access-token");
    
      if (token && token == "") {
        this.navCtrl.push(LoginPage );
      }
    
  }

  ionViewDidLoad() {
    this.getMyCart();
  }

  checkOut() {
    if (this.hasLoggedIn) {
      let totalPrice = this.gettotalprice();
      this.navCtrl.push(CheckoutPage, {
        total: totalPrice,
        productCount: this.productArr.length
      });
    } else {
      let msg = this.getCurrentLAnguage() == 'en' ? 'You must login first.' : 'سجل دخول اولا';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }
  }




  getCurrentLAnguage() {
    return this.appSettingsProvider.Language;
  }

  increaseQuantity(cartid, quantity) {
    this.cartSrvc.changeQuantity(cartid, (quantity + 1)).then(data => {
      this.getMyCart();
    }, err => {
      console.log(err);
    });
  }

  decreaseQuantity(cartid, quantity) {
    this.cartSrvc.changeQuantity(cartid, (quantity - 1)).then(data => {
      this.getMyCart();
    }, err => {
      console.log(err);
    });
  }

  getMyCart() {
    let loading = this.loadingCtrl.create({ content: '' });
    loading.present();
    this.cartSrvc.getMyCart().then(data => {
      this.productArr = data;
      loading.dismiss();
    }, err => {
      loading.dismiss();
    });
  }

  removefromcart(cartid) {
    this.cartSrvc.removeFromCart(cartid).then(data => {
      this.getMyCart();
    }, err => {
      console.log(err);
    });
  }

  updateQuantity(cartid, quantity) {
    this.cartSrvc.changeQuantity(cartid, quantity).then(data => {
      this.getMyCart();
    }, err => {
      console.log(err);
    });
  }

  gettotalprice() {
    var total = 0;
    for (var i = 0; i < this.productArr.length; i++) {
      var product = this.productArr[i];
      total += (product.price * product.quantity);
    }
    return total;
  }

  clearCart() {
    this.cartSrvc.clearCart().then(data => {
      this.getMyCart();
    }, err => {
      console.log(err);
    });
  }

  toggleWishList(obj) {
    this.productSrvc.addToFavourite(obj).then(data => {
      obj = data;
    }, err => {
      console.log(err);
    });
  }
}
