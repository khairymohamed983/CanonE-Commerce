
import { SendMessagePage } from '../send-message/send-message';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { HTTP } from '../../../node_modules/@ionic-native/http';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions
 } from '@ionic-native/google-maps';
 
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
   //providers: [GoogleMaps]
})
export class ContactUsPage {
  map: GoogleMap;
   mapElement: HTMLElement;
  language;

  contactUsInfo: any = {
    phone: '',
    name: '',
    address: '',
    email: '',
    fax: '',
    map: {
      lat: '',
      lng: ''
    }
  }


  constructor(public storage: Storage, public navCtrl: NavController,public navParams: NavParams, public HTTP: HTTP) {

    this.getCurrentLang();
    this.getContactUsData();

  }

  sendMessage() {
    this.navCtrl.push(SendMessagePage);
  }



  getContactUsData() {

    this.HTTP.get('http://alsayershop.com/public/api/contactus',{},{}).then(res => {
      this.contactUsInfo = JSON.parse(res.data).data;
      console.log(this.contactUsInfo);
    });

  }

  getCurrentLang() {
    this.storage.get('lang').then((val) => {
      if (val == null) {
        this.language = 'en'
      } else {
        this.language = val;
      }
    })
  }

  ionViewDidLoad() {
    this.loadMap();
  }
  ionViewWillEnter(){
    this.loadMap();    
  }

  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat:29.2933271,
          lng:47.9244662
        },
        zoom: 10,
        tilt: 30
      }
    };

    this.map = GoogleMaps.create('map', mapOptions);

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');
        
        // Now you can use all methods safely.
        this.map.addMarker({
            title: 'Ionic',
            icon: 'blue',
            animation: 'DROP',
            position: {
              lat: 29.2933271,
              lng: 47.9244662
            }
          })
          .then(marker => {
            marker.on(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(() => {
                alert('clicked');
              });
          });

      });
  }


  // loadMap() {
  //   this.mapElement = document.getElementById('map');
  //   let mapOptions: GoogleMapOptions = {
  //     camera: {
  //       target: {
  //         lat: ,
  //         lng: 
  //       },
  //       zoom: 18,
  //       tilt: 30
  //     }
  //   };
  //   this.map = GoogleMaps.create(this.mapElement, mapOptions);
  //   // Wait the MAP_READY before using any methods.
  //   this.map.one(GoogleMapsEvent.MAP_READY)
  //     .then(() => {
  //       console.log('Map is ready!');
  //       // Now you can use all methods safely.
  //       this.map.addMarker({
  //         title: 'Alsayer Shop',
  //         icon: 'blue',
  //         animation: 'DROP',
  //         position: {
  //           lat:this.contactUsInfo.map.lat,
  //           lng:this.contactUsInfo.map.lng
  //         }
  //       })
  //         .then(marker => {
  //           marker.on(GoogleMapsEvent.MARKER_CLICK)
  //             .subscribe(() => {
  //               console.log('clicked');
  //             });
  //         });
  //     });
  // }
} 