import { MyApp } from './../../app/app.component';
import { HomePage } from '../home/home';
import { RegisterPage } from '../register/register';


import { Component } from '@angular/core';
import {  LoadingController, NavController, Events, ToastController, AlertController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { NgForm } from '@angular/forms';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { HTTP } from '../../../node_modules/@ionic-native/http';

@Component({
  selector: 'page-login-with-email',
  templateUrl: 'login-with-email.html',
})
export class LoginWithEmailPage {


  signinData = {
    password: "",
    username: "",
    client_id: "2",
    grant_type: 'password',
    scope: '*',
    client_secret: 'VpkTtQe71mg2Cioua0Igr4KVUzeCGLNGsNMX2KQA'
  };




  constructor(public navCtrl: NavController, public navParams: NavParams,
    public HTTP: HTTP,
    public events: Events, public loadingCtrl: LoadingController,
    public storage: Storage,
    public toastCtrl: ToastController, public alertCtrl: AlertController,
    public appSettingsProvider: AppSettingsProvider, public myApp:MyApp) {

    this.signinData.username = "";

  }






  signin(form: NgForm) {
    
let data={
  'username':this.signinData.username,
'password':this.signinData.password,
'client_id':this.signinData.client_id,
'client_secret':this.signinData.client_secret,
'grant_type':this.signinData.grant_type
}

  
    this.HTTP.post('http://alsayershop.com/public/api/login', data, {
        'Accept-Language': "ar"
    })
      .then(res => {
        let ress=JSON.parse(res.data).data;

console.log("ress:"+JSON.parse(res.data).data);
console.log("accesss:"+ress.access_token);


        this.storage.set('hasLoggedIn', true);
        this.storage.set('access_token', ress.access_token);
        window.localStorage.setItem("access-token",ress.access_token);

        this.navCtrl.setRoot(HomePage);
        this.myApp.isLogedId=true;
        this.myApp.setSideMenu();

      }, err => {
      
        let alert = this.alertCtrl.create({title: '',subTitle:'عفوا حذث خطاء'  });
        alert.present();

      });

  }

 
  openRegisterPage() {
    this.navCtrl.push(RegisterPage);
  }

  login() {
    this.navCtrl.setRoot(HomePage);
  }

  // openForgetPassPage(){
  //   this.navCtrl.setRoot(HomePage);
  // }


  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }

}
