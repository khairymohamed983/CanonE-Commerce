
import { Component } from '@angular/core';
import { NavController, NavParams,Events  } from 'ionic-angular';

@Component({
  selector: 'page-sort-popover',
  templateUrl: 'sort-popover.html',
})
export class SortPopoverPage {

  constructor(public navCtrl: NavController,
     public navParams: NavParams, public events:Events) {
  }

  SortPrice(val) {
    this.events.publish('priceSort', val);
  }

  SortRating(val) {
    this.events.publish('ratingSort', val);
  }


}
