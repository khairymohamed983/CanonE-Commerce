import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-my-orders',
  templateUrl: 'my-orders.html',
})
export class MyOrdersPage {

  pageType: string = "Pending"; // default button
  pendingArr:number[] = new Array(2) ;    
  historyArr:number[] = new Array(2) ;    
  
  constructor(public navCtrl: NavController, public navParams: NavParams,public storage:Storage) {
  }

  ionViewCanEnter() {
    this.storage.get('hasLoggedIn').then((val) => {
      if (val == false) {
        this.navCtrl.push(LoginPage);
      }
    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad MyOrdersPage');
  }

}
