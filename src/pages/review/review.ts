import { AddReviewPopoverPage } from '../add-review-popover/add-review-popover';
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';



@Component({
  selector: 'page-review',
  templateUrl: 'review.html',
})
export class ReviewPage {

  rate = 3;
  productId: number;
  lstReviews= [];
  title:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController,public appSettingsProvider:AppSettingsProvider) {

    this.productId = this.navParams.get("productId");
    this.lstReviews = this.navParams.get("lstReviews");
  }

  ionViewDidLoad() { 
    this.title=this.getCurrentLang()=='ar'?'عفوا لايوجد اراء على هذا المنتج':'sorry ... their is no feedbacks on this product';
  }


  addReviewPopover(productId) {
    let popover = this.popoverCtrl.create(AddReviewPopoverPage, { productId: productId });
    popover.present({});
    popover.onDidDismiss((data , role)=>{
      if(!isNaN(data))
      {
        //this.lstReviews.push(data);
      }
     
    });
  }

  
  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }



}
