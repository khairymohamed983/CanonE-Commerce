
import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, LoadingController, ToastController } from 'ionic-angular';
import { SortPopoverPage } from '../sort-popover/sort-popover';
import { SearchPopoverPage } from '../search-popover/search-popover';
import { ProductProvider } from '../../providers/product/product';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { ProductPage } from '../product/product';
import { Storage } from '@ionic/storage';
import { CartProvider } from '../../providers/cart/cart';

@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  isPortrait: boolean = true;
  selectedCategoryName: string = "";
  categoryid: number;
  lstProducts: any[] = [];
  emptyMsg: string;
  lstSubCategories: any[] = [];
  subcatid: number;
  selectedSubCat: any;
  hasLoggedIn: boolean;
  selectOptions: object;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController,
    public productSrvc: ProductProvider,
    public loadingCtrl: LoadingController,
    public appSettingsProvider: AppSettingsProvider,
    public storage: Storage,
    public cartSrvc: CartProvider,
    private toastCtrl: ToastController) {

    this.categoryid = this.navParams.get("categoryid");
    this.selectedCategoryName = this.navParams.get("categoryname");
    if (this.navParams.get("categoryid"))
      this.getProductsPerCategory(this.categoryid);
      else
      {
        this.productSrvc.getProducts("","","",this.navParams.get("data"),"").then(res=>{
          this.lstProducts = res.products;
          this.lstSubCategories = res.subcategories;
        });
      }



    this.storage.get('hasLoggedIn').then((val) => { this.hasLoggedIn = val; });
  }



  ngAfterViewInit() {
    // this.getProductsPerCategory(this.categoryid);
  }

  selectSubCat() {
    this.lstProducts = this.selectedSubCat.products.data;

  }





  getProductsPerCategory(catId) {

    let loading = this.loadingCtrl.create({ content: '' });
    loading.present();
    this.productSrvc.getCategoryProducts(catId).then(data => {
      console.log("str data: " + JSON.stringify(data));

      this.lstProducts = data.products;
      this.lstSubCategories = data.subcategories;
      loading.dismiss();
    }, err => {
      console.log(err);
    });

  }

  gotoProductPage(id) {
    this.navCtrl.push(ProductPage, {
      id: id
    });
  }


  setPortrait(param) {
    this.isPortrait = param;
  }

  openSortPopover(myEvent) {
    let popover = this.popoverCtrl.create(SortPopoverPage, { cssClass: 'custom-popover' });
    console.log('my event', myEvent);
    // to get data on popover dissmis
    popover.present({
      ev: myEvent
    });

  }

  search(myEvent) {
    let popover = this.popoverCtrl.create(SearchPopoverPage, {}, { cssClass: 'searchPopover' });
    popover.present({
      ev: myEvent
    });
  }


  getCurrentLAnguage() {
    return this.appSettingsProvider.Language;
  }

  addToFavourite(e, obj) {
    // check if logged in first
    if (this.hasLoggedIn) {
      this.productSrvc.addToFavourite(obj).then(data => {
        obj = data;
      }, err => {
        console.log(err);
      });
      e.stopPropagation();
    } else {
      let msg = this.getCurrentLAnguage() == 'en' ? 'You must login first.' : 'سجل دخول اولا';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }
  }



  addtocart(e, id) {
    if (this.hasLoggedIn) {
      this.cartSrvc.addToCart(id).then(data => {
        if (data.status == '200') { // success
          let msg = this.getCurrentLAnguage() == 'en' ? 'Added Successfuly.' : 'تمت الاضافة بنجاح';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        } else if (data.status == '400') { // added before
          let msg = this.getCurrentLAnguage() == 'en' ? 'Product Added Before.' : 'تم اضافة المنتج من قبل';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        }
      }, err => {
        let msg = this.getCurrentLAnguage() == 'en' ? 'Product Added Before.' : 'تم اضافة المنتج من قبل';
        let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
        toast.present();
      });
      e.stopPropagation();
    } else {
      let msg = this.getCurrentLAnguage() == 'en' ? 'You must login first.' : 'سجل دخول اولا';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }
  }




}
