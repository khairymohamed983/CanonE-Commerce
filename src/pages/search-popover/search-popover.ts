import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SearchPage } from '../search/search';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';

@Component({
  selector: 'page-search-popover',
  templateUrl: 'search-popover.html',
})
export class SearchPopoverPage {

  isInputClear=true;
  searchInput:string='';

  constructor(public appSettingsProvider:AppSettingsProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  gotoSearchPage() {
    this.navCtrl.push(SearchPage);
  }

  clearSearchBar() {
    this.searchInput = '';
  }

  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }

}
