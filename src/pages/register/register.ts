// import { HomePage } from './../home/home';
import { Component } from '@angular/core';
import {  LoadingController, NavController, Events, ToastController, AlertController, NavParams } from 'ionic-angular';
import { LoginPage } from "../login/login";
import { Storage } from '@ionic/storage';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { NgForm } from '@angular/forms';
import { HomePage } from '../home/home';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  isAcceptingAgreemnent: boolean = false;
  userData: any = {
    password: "",
    name: "",
    mobile: "",
    email: "",
    firstName: '',
    lastName: '',
    gender: 'male',
    client_id: 2,
    grant_type: 'password',
    scope: '*',
    client_secret: 'VpkTtQe71mg2Cioua0Igr4KVUzeCGLNGsNMX2KQA'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http,
    public events: Events, public loadingCtrl: LoadingController,
    public storage: Storage,
    public toastCtrl: ToastController, public alertCtrl: AlertController) {
  }

  login() {
    this.navCtrl.push(LoginPage);
  }


  presentAlert(title) {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: title,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  register(form: NgForm) {

    let body = new URLSearchParams();
    body.set('name', this.userData.firstName + ' ' + this.userData.lastName);
    body.set('password', this.userData.password);
    body.set('email', this.userData.email);
    body.set('mobile', this.userData.mobile);
    body.set('client_id', this.userData.client_id);
    body.set('client_secret', this.userData.client_secret);
    body.set('gender', this.userData.gender);
    body.set('grant_type', this.userData.grant_type);

    //console.log(body.toString());

    this.http.post('http://alsayershop.com/public/api/register', body, {
      headers: new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json', 'Accept-Language': 'ar'
      })
    }).subscribe(res => {
      console.log(res);
      if (res.status == 200) {
        this.storage.set('hasLoggedIn', true).then(hasLogged=>{
          this.storage.set('access_token', res.json().data.access_token).then(access=>{
            this.navCtrl.setRoot(HomePage);
          });
        });
      }
    }, err => {
      debugger;
      if (err.json().status == 422) {
        let message = "";
        if (isNaN(err.json().errors.email)) {
          message += err.json().errors.email[0];
          message+=" ";
        }

        if (isNaN(err.json().errors.mobile)) {
          message += err.json().errors.mobile[0];
        }
        
        let alert = this.alertCtrl.create({title: '',subTitle: message, buttons: ['تم']});
        alert.present();


      }

    });

  }
}
