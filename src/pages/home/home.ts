import { Storage } from '@ionic/storage';
import { SearchPage } from '../search/search';
import { ProductPage } from '../product/product';
import { SpecialsPage } from '../specials/specials';
import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController, Slides, ToastController } from 'ionic-angular';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { Http, RequestOptions } from '@angular/http';
import { HomeProvider } from '../../providers/home/home';
import { ProductProvider } from '../../providers/product/product';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Slides) mainslides: Slides;
  @ViewChild(Slides) brandsslides: Slides;


  isFavourite: boolean = false;
  isInputClear = true;
  searchInput: string = '';
  rate = 3;

  hasLoggedIn: boolean;

  mainslider = [];
  brandsslider = [];
  latest = [];
  specialArr = [];
  bestseallers = [];
  options = new RequestOptions();
  loaded=false;
  loaded2=false;
  loaded3=false;

  constructor(public appSettingsProvider: AppSettingsProvider,
    public navCtrl: NavController, public alertCtrl: AlertController,
    public http: Http, public storage: Storage, public homeSrvc: HomeProvider, 
    public productSrvc: ProductProvider, public toastCtrl: ToastController) {


    this.storage.get('hasLoggedIn').then((val) => { this.hasLoggedIn = val; });
    
    this.loadAllLists();
  }

  ngAfterViewInit() { }

  ionViewDidLoad() {
    
  }

  
  loadAllLists() {
    // get Main slider data 
    this.homeSrvc.getMainSlider().then(data => {
      console.log("mainslider"+ JSON.stringify(data));
        this.mainslider = data;
        this.loaded=true;
      });

    // get brands
    this.homeSrvc.getBrands().then(data => {

      console.log("brands"+ data.data);
      
        this.brandsslider = data;
        this.loaded2=true;
      });


    //get featured products that require 
      this.homeSrvc.getProducts("").then(data => {

        this.latest = data.latestProducts.data;
        this.specialArr = data.specialArr.data;
        this.bestseallers = data.bestseallers.data;
        this.loaded3=true;
      }, err => {
        console.log(err);
      });
   

   

  }

  starClicked(value) {
    console.log("Avaliaram em :", value);
  }

  openSpecialsPage(type) {
    this.navCtrl.push(SpecialsPage, {
      type: type
    });
  }

  gotoSearchPage() {
    this.navCtrl.push(SearchPage,{data:this.searchInput});
  }

 

  gotoProductPage(id) {
    this.navCtrl.push(ProductPage, {
      id: id
    });
  }

  clearSearchBar() {
    this.searchInput = '';
  }

  addToFavourite(e, obj) {
    if (this.hasLoggedIn) {
      this.productSrvc.addToFavourite(obj).then(data => {
        obj = data;
      }, err => {
        console.log(err);
      });
      e.stopPropagation();
    } else {
      e.stopPropagation();
      let msg = this.getCurrentLang() == 'ar' ? 'لابد من تسجيل الدخول أولا' : 'you must login first';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();

    }
  }

  isFavouriteProduct() {
    return this.isFavourite;
  }

  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }

}
