import { SpecialsPage } from '../specials/specials';
import { MyOrdersPage } from '../my-orders/my-orders';
import { HomePage } from '../home/home';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, App } from 'ionic-angular';
import { MyAccountPage } from '../my-account/my-account';
import { CartPage } from '../cart/cart';
import { WishlistPage } from '../wishlist/wishlist';

@Component({
  selector: 'page-more-popver',
  templateUrl: 'more-popver.html',
})
export class MorePopverPage {

  popvarArr;

  constructor(public navCtrl: NavController, public navParams: NavParams,public appCtrl: App,public viewCtrl: ViewController) {
    this.popvarArr = this.navParams.get('popvarArr');
    console.log(this.navParams.get('popvarArr'));
  }

  openPage(page) {
    console.log(page);
    if(page == 'Home') {
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot(HomePage);

    } else if(page == 'My Account') {
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot(MyAccountPage);
    } else if(page == 'Cart') {
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot(CartPage);
    } else if(page == 'Wishlist') {
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot(WishlistPage);
    } else if(page == 'My Orders') {
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot(MyOrdersPage);
    } else if(page == 'Specials') {
      this.viewCtrl.dismiss();
      this.appCtrl.getRootNav().setRoot(SpecialsPage);
    }
  }

}
