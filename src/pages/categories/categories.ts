import { SearchPage } from './../search/search';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http';
// import { SpecialsPage } from '../specials/specials';
import { HTTP } from '../../../node_modules/@ionic-native/http';


@Component({
  selector: 'page-categories',
  templateUrl: 'categories.html',
})
export class CategoriesPage {
  categories:any[] = [];    

  constructor(public navCtrl: NavController, public navParams: NavParams,public HTTP:HTTP) {

   
  }

  ionViewDidLoad() {
    this.getallcategories();
  }


 
getallcategories(){

  this.HTTP.get('http://alsayershop.com/public/api/categories',{},{'Accept-Language':'ar'})
  .then(res => {
    this.categories=JSON.parse(res.data).data;
  });
}



getproducts(catId,name){
this.navCtrl.push(SearchPage,{
  categoryid:catId,
  categoryname:name

});
}



}
