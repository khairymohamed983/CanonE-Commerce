import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, App, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core'; 

@Component({
  selector: 'page-language-popver',
  templateUrl: 'language-popver.html',
})
export class LanguagePopverPage {

  constructor(public events: Events, public translateService:TranslateService,public storage:Storage,public navCtrl: NavController,public appCtrl: App, public navParams: NavParams,public viewCtrl: ViewController) {
  }

  setEnglishLanguage() {
    this.storage.set('lang','en').then((val)=>{
      this.viewCtrl.dismiss();
      this.translateService.use('en');
      this.events.publish('lang', 'en');
      // this.appCtrl.getRootNav().setRoot(SettingsPage);
      // window.location.reload();
    })
  }

  setArabicLanguage() {
    this.storage.set('lang','ar').then((val)=>{
      this.viewCtrl.dismiss();
      this.translateService.use('ar');
      this.events.publish('lang', 'ar');
      // this.appCtrl.getRootNav().setRoot(SettingsPage);
      // window.location.reload();
    })
  }

}
