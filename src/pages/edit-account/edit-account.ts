// import { MyApp } from './../../app/app.component';
// import { Http, RequestOptions, Headers,URLSearchParams } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { ToastController, AlertController } from 'ionic-angular';
import { AccountProvider } from '../../providers/account/account';
@Component({
  selector: 'page-edit-account',
  templateUrl: 'edit-account.html',
})

export class EditAccountPage {

  base64Image = "";
  hasLoggedIn: boolean;

  userData: any = {
    name: "",
    mobile: "",
    email: "",
    username: '',
    gender: '',
    image: '',
    dateofbirth: ''
  };

  constructor(public navCtrl: NavController, public storage: Storage, public accSrvc: AccountProvider, public loadCtrl: LoadingController,
    public navParams: NavParams, private camera: Camera, public alertCtrl: AlertController,
    public appSettingsProvider: AppSettingsProvider, public toastCtrl: ToastController) {
    this.storage.get('hasLoggedIn').then((val) => { this.hasLoggedIn = val; });
    let loading = this.loadCtrl.create({
      content: ''
    });
    loading.present();
    this.accSrvc.getUserData().subscribe(res => {
      this.userData = res;
      loading.dismiss();

    }, err => {
      debugger;
      loading.dismiss();
      console.log(err);
    });
  }

  updateprofile() {
    let loading = this.loadCtrl.create({
      content: ''
    });
    loading.present();
    this.accSrvc.updateUserData(this.userData, this.base64Image).subscribe(res => {
      loading.dismiss();
      let msg = this.getCurrentLang() == 'en' ? 'Data Saved Successfuly.' : 'تمت التعديل بنجاح';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }, err => {
      loading.dismiss();      
      let toast = this.toastCtrl.create({ message: err.json().message, duration: 3000, position: 'bottom' });
      toast.present();
    });
  }

  deleteaccount() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Account Delete',
      message: 'Do you want to Permently Delete your Account?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm Account Delete',
          handler: () => {
            let loading = this.loadCtrl.create({
              content: ''
            });
            loading.present();
        
            this.accSrvc.deleteAccount().subscribe(res => {
              loading.dismiss();
              let btnName = this.getCurrentLang() == 'ar' ? 'تم' : 'ok';
              let msg = this.getCurrentLang() == 'ar' ? 'تم حذف الحساب بنجاح' : 'The Account has been deleted';
              let alert = this.alertCtrl.create({ title: '', subTitle: msg, buttons: [btnName] });
              alert.present();
            });
          }
        }
      ]
    });
    alert.present();
  }

  uploadImage() {
    let loading = this.loadCtrl.create({
      content: ''
    });
    loading.present();
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      quality: 40,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }).then((imageData: any) => {
      try {
        this.base64Image = 'data:image/jpeg;base64,' + imageData;
        this.userData.image = this.base64Image;
        loading.dismiss();
      } catch (err) {
        alert(err);
        loading.dismiss();        
      }
    }, (err: any) => {
      alert(err);
      loading.dismiss();
      console.log(err);
    });
  }

  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }


}
