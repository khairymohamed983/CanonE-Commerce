// import { MyApp } from './../../app/app.component';
import { Storage } from '@ionic/storage';
import { WishlistPage } from '../wishlist/wishlist';
import { CartPage } from '../cart/cart';
import { Component } from '@angular/core';
import { Nav, NavController, NavParams } from 'ionic-angular';
import { EditAccountPage } from "../edit-account/edit-account";
import { MyOrdersPage } from '../my-orders/my-orders';
import { Http, RequestOptions, Headers } from '@angular/http';
import { HomePage } from '../../pages/home/home';
import { LoginPage } from '../../pages/login/login';


@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {


  accessToken = "";
  userData: any = {
    name: "",
    email: "",
    image: ""
  };


  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public http: Http, public nav: Nav) {

    this.storage.get('access_token').then((val) => {
      this.accessToken = val;
      this.getUserData();
    });
  }


  ionViewCanEnter() {
    this.storage.get('hasLoggedIn').then((val) => {
      if (val == false) {
        this.navCtrl.push(LoginPage);
      }
    });
  }

  
  getUserData() {

    let headers = new Headers({ 'Accept': 'application/json', 'Authorize': this.accessToken });
    let options = new RequestOptions({ headers: headers });

    this.http.get('http://alsayershop.com/public/api/user', options).subscribe(res => {
      this.userData = res.json().data;
    });
  }

  logout() {
    this.http.get('http://alsayershop.com/public/api/logout', {
      headers: new Headers({ 'Authorize': this.accessToken, 'Accept': 'application/json' })
    }).subscribe(res => {
      this.storage.set('hasLoggedIn', false);
      this.storage.set('access_token', '');
      this.nav.setRoot(HomePage);
      // reload nav menu
      // this.isLogedId=false;
      // this.setSideMenu();
      // this.myapp.isLogedId = false;
      // this.myapp.setSideMenu();

    }, err => {
      this.nav.push(LoginPage);
    });

  }
 
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAccountPage');
  }

  editAccount() {
    this.navCtrl.push(EditAccountPage);
  }

  gotoCart() {
    this.navCtrl.setRoot(CartPage);
  }

  gotoWishlist() {
    this.navCtrl.setRoot(WishlistPage);
  }

  gotoMyOrders() {
    this.navCtrl.setRoot(MyOrdersPage);
  }

}
