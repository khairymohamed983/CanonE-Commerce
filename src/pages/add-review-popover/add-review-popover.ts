import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, ToastController } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
// import { RequestOptions, Headers, URLSearchParams } from '@angular/http';
// import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
// import { LoginPage } from '../../pages/login/login';
// import { ReviewPage } from '../review/review';
import { ProductProvider } from '../../providers/product/product';
@Component({
  selector: 'page-add-review-popover',
  templateUrl: 'add-review-popover.html',
})

export class AddReviewPopoverPage {
  hasLoggedIn: boolean;
  access_token: string;
  addReviewModel = {
    productId: this.navParams.get("productId"),
    review: "",
    rating: 1
  };

  rate;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public appSettingsProvider: AppSettingsProvider, public alertCtrl: AlertController,
    public productSvc: ProductProvider, public storage: Storage, private toastCtrl: ToastController) {
    this.storage.get('hasLoggedIn').then((val) => { this.hasLoggedIn = val; });
    this.storage.get('access_token').then((val) => { this.access_token = val; });
  }

  close() {
    this.viewCtrl.dismiss();
  }

  onModelChange(myEvent) {
    this.rate = myEvent;
  }
 
  addReview(form: NgForm) {
    if (this.addReviewModel.review == '') {
      let msg = this.getCurrentLang() == 'ar' ? 'لابد من ادخال التقيم' : 'feedback is required';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    } else {
      // check if user loged in 
      if (this.hasLoggedIn) {
        // add review api here 
        this.productSvc.addReview(this.addReviewModel).then(data => {
          let msg = this.getCurrentLang() == 'ar' ? 'تم اضافة التقيم بنجاح' : 'feedback added successfuly';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
          this.viewCtrl.dismiss(data);
        }, err => {
          console.log(err);
        });
      } else {
        //this.navCtrl.push(LoginPage);
        let msg = this.getCurrentLang() == 'ar' ? 'لابد من تسجيل الدخول أولا' : 'you must login first';
        let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
        toast.present();
      }
    }
  }

  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }
}
