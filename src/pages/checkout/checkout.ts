import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DeliveryDatailsPage } from '../delivery-datails/delivery-datails';

@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {


productsCount:number;
totalPrice:number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.productsCount = this.navParams.get("productCount");
    this.totalPrice = this.navParams.get("total");
    
    
  }



  gotoDeliveryDetails() {
    this.navCtrl.push(DeliveryDatailsPage,{
      total: this.totalPrice,
      productCount:this.productsCount
    });
  }

}
