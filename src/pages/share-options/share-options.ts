import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-share-options',
  templateUrl: 'share-options.html',
})
export class ShareOptionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtr:ViewController,private socialSharing: SocialSharing) {
    
  }

  copyLink() {
    this.viewCtr.dismiss();
  }

  shareToFB() {
    this.socialSharing.shareViaFacebook('producr','dddddd','http://www.facebook.com').then(()=>{
      //success
      this.viewCtr.dismiss();
    }).catch(() =>{
      //error;
    });
    
  }

  shareToTwitter() {
    this.socialSharing.shareViaTwitter('producr','dddddd','http://www.facebook.com').then(()=>{
      //success
      this.viewCtr.dismiss();
    }).catch(() =>{
      //error;
    });
    
  }

  shareToGoogle() {
    this.socialSharing.shareViaEmail('Body', 'Subject', ['recipient@example.org']).then(() => {
      // Success!
      this.viewCtr.dismiss();
    }).catch(() => {
      // Error!
  });
    
  }

  shareToWhats() {
    this.socialSharing.shareViaWhatsApp('producr','dddddd','http://www.facebook.com').then(()=>{
      //success
      this.viewCtr.dismiss();
    }).catch(() =>{
      //error;
    });
    
  }

  shareToTumblr() {
    this.viewCtr.dismiss();
  }

  shareToLinkedin() {
    this.viewCtr.dismiss();
  }

  more() {
    this.viewCtr.dismiss();
  }

  dismissModal() {
    this.viewCtr.dismiss();
  }

}
