import { HomeProvider } from './../../providers/home/home';
import { AppSettingsProvider } from './../../providers/app-settings/app-settings';

import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';



@Component({
  selector: 'page-send-message',
  templateUrl: 'send-message.html',
})
export class SendMessagePage {


  msgData = {
    name: "",
    email: "",
    message: ""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public homeSrv: HomeProvider, public appSettingsProvider: AppSettingsProvider,
    public toastCtrl: ToastController) {
  }

  sendEmail() {
    this.homeSrv.sendMsg(this.msgData).subscribe(data => {
      let toast = this.toastCtrl.create({ message: data.message, duration: 3000, position: 'bottom' });
      toast.present();
      this.msgData.email = '';
      this.msgData.message = '';
      this.msgData.name = '';
      this.navCtrl.pop();
    }, err => {
      console.log(err);
    });

  }

  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }


}
