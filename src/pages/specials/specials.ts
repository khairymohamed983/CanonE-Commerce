import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ToastController, Events } from 'ionic-angular';
import { SortPopoverPage } from '../sort-popover/sort-popover';
import { SearchPopoverPage } from '../search-popover/search-popover';
import { Storage } from '@ionic/storage';
import { Http, RequestOptions } from '@angular/http';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { LoginPage } from '../login/login';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ProductPage } from '../product/product';
import { ProductProvider } from '../../providers/product/product';
import { CartProvider } from '../../providers/cart/cart';
// import { enableDebugTools } from '@angular/platform-browser/src/browser/tools/tools';

@Component({
  selector: 'page-specials',
  templateUrl: 'specials.html',
})
export class SpecialsPage {

  isPortrait: boolean = true;
  isFavourite: boolean = false;
  isEnglish: boolean;
  portraitArr: number[] = new Array(6);
  rate = 2;
  type: any;
  lstProducts: any[] = [];
  title: string = "";
  categoryid: number;

  price: string = '';
  rating: string = '';
  name: string = '';
  page: number = 1;

  hasLoggedIn: boolean;
  access_token: string;
  refresh_token: string;
  options = new RequestOptions();



  constructor(public storage: Storage, public http: Http, public appSettingsProvider: AppSettingsProvider,
    public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public cartSrvc: CartProvider,
    public productSrvc: ProductProvider, public events: Events) {
    this.isEnglish = this.getCurrentLang() == 'ar' ? false : true;
    this.type = this.navParams.get("type");
    this.title = this.type == 'latest' ? (this.isEnglish ? 'Latest' : 'المضافة حديثا') : (this.type == 'sells' ? (this.isEnglish ? 'Best Sales' : 'افضل المبيعات') : (this.isEnglish ? 'Special' : 'العروض الخاصة'));

    this.categoryid = this.navParams.get("categoryid");
    if (!isNaN(this.categoryid)) {
      this.title = this.navParams.get("categoryname");
      this.getcategoryproducts(this.categoryid);
    } else {
      this.getProducts(this.type);
    }
    this.storage.get('hasLoggedIn').then((val) => { this.hasLoggedIn = val; });


    events.subscribe('priceSort', (val) => {
      this.price = val;
      this.rating = '';
      this.getProducts();
      // this.viewCtrl.dismiss();
    });


    events.subscribe('ratingSort', (val) => {
      this.rating = val;
      this.price = '';
      this.getProducts();
      // this.viewCtrl.dismiss();
    });


  }

  addToFavourite(e, obj) {
    // check if logged in first
    if (this.hasLoggedIn) {
      this.productSrvc.addToFavourite(obj).then(data => {
        obj = data;
      }, err => {
        console.log(err);
      });
      e.stopPropagation();
    } else {
      this.navCtrl.push(LoginPage);
    }
  }

  ngAfterViewInit() {
    //this.getProducts();
  }

  starClicked(value) {
    console.log("Avaliaram em :", value);
  }

  getProducts(type = this.type, price = this.price, rating = this.rating, name = this.name, page = this.page) {
    this.productSrvc.getProducts(type, price, rating, name, page).then(data => {
      this.lstProducts = data;
    }, err => {
      console.log(err);
    });
  }

  getcategoryproducts(id) {
    this.productSrvc.getCategoryProducts(id).then(data => {
      this.lstProducts = data;
    }, err => {
      console.log(err);
    });
  }
  getCurrentLAnguage() {
    return this.appSettingsProvider.Language;
  }

  addtocart(e, id) {
    if (this.hasLoggedIn) {
      this.cartSrvc.addToCart(id).then(data => {
        if (data.status == '200') { // success
          let msg = this.getCurrentLAnguage() == 'en' ? 'Added Successfuly.' : 'تمت الاضافة بنجاح';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        } else if (data.status == '400') { // added before
          let msg = this.getCurrentLAnguage() == 'en' ? 'Product Added Before.' : 'تم اضافة المنتج من قبل';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        }
      }, err => {
        let msg = this.getCurrentLAnguage() == 'en' ? 'Product Added Before.' : 'تم اضافة المنتج من قبل';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
      });
      e.stopPropagation();
    } else {
      let msg = this.getCurrentLAnguage() == 'en' ? 'You must login first.' : 'سجل دخول اولا';
      let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
      toast.present();
    }
  }


  setPortrait(param) {
    this.isPortrait = param;
  }

  onModelChange(eve) {
    console.log(eve);
  }



  gotoProductPage(id) {
    this.navCtrl.push(ProductPage, {
      id: id
    });
  }

  isFavouriteProduct() {
    return this.isFavourite;
  }

  openSortPopover(myEvent) {
    let popover = this.popoverCtrl.create(SortPopoverPage, {}, { cssClass: 'my-custom-popover' });
    // to get data on popover dissmis
    popover.present({ ev: myEvent });



  }

  search(myEvent) {
    let popover = this.popoverCtrl.create(SearchPopoverPage);
    popover.present({ ev: myEvent });

    // this.navCtrl.push(SearchPage);
  }


  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }

}
