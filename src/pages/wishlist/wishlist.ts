import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { LoginPage } from '../login/login';
import { ProductPage } from '../product/product';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';
import { CartProvider } from '../../providers/cart/cart';
import { ProductProvider } from '../../providers/product/product';


@Component({
  selector: 'page-wishlist',
  templateUrl: 'wishlist.html',
})
export class WishlistPage {

  rate = 3;
  wishlidtArr: any[] = [];
  hasLoggedIn: boolean;
  emptyMsg:string="";

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public http: Http, public storage: Storage,
    public productSrvc: ProductProvider,public loadingCtrl:LoadingController,
    public cartSrvc: CartProvider, public toastCtrl: ToastController,
    public appSettingsProvider: AppSettingsProvider, public alertCtrl: AlertController) {

    this.storage.get('hasLoggedIn').then((val) => { this.hasLoggedIn = val; });
    this.emptyMsg=this.appSettingsProvider.language=='ar'?'عفوا لا توجد منتجات مضافة الى القائمة المفضلة لديك':'Sorry  their is no products added to your withlist ';
    this.getMyWishList();
  }


  ionViewCanEnter() {
    this.storage.get('hasLoggedIn').then((val) => {
      if (val == false) {
        this.navCtrl.push(LoginPage);
      }
    });
  }

  
  getMyWishList() {
    let loading = this.loadingCtrl.create({ content: '' });
    loading.present();
    this.productSrvc.getMyWishList().then(data=>{
      this.wishlidtArr = data;
      loading.dismiss();
    },err=>{
      console.log(err);
      loading.dismiss();
    });
  }


  getCurrentLAnguage() {
    return this.appSettingsProvider.Language;
  }


  
  addToCart(e, productId) {
    if (this.hasLoggedIn) {
      this.cartSrvc.addToCart(productId).then(data => {
        if (data.status == '200') { // success
          let msg = this.getCurrentLAnguage() == 'en' ? 'Added Successfuly.' : 'تمت الاضافة بنجاح';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        } else if (data.status == '400') { // added before
          let msg = this.getCurrentLAnguage() == 'en' ? 'Product Added Before.' : 'تم اضافة المنتج من قبل';
          let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
          toast.present();
        }
      }, err => {
        let msg = this.getCurrentLAnguage() == 'en' ? 'Product Added Before.' : 'تم اضافة المنتج من قبل';
        let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
        toast.present();
      });
      e.stopPropagation();
    } else {
      this.navCtrl.push(LoginPage);
    }

  }


  gotoproduct(id) {
    this.navCtrl.push(ProductPage, { id: id });
  }

  removeWishlist(e, id) {
    this.productSrvc.removeFromWishlist(id).then(data => {
      if (data.status == '200') {
        this.getMyWishList();
      }
    }, err => {
      console.log(err);
    });
    e.stopPropagation();
  }


  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }


}