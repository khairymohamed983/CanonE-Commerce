import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, Events } from 'ionic-angular';
import { LanguagePopverPage } from '../language-popver/language-popver';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  language;
  pushNotification = false;
  newsLetter = false;

  constructor(public events: Events, public storage: Storage, public navCtrl: NavController, public navParams: NavParams,
    public popoverCtrl: PopoverController) {
    this.getCurrentStorage();
    this.setLAnguageFromEvents();
  }

  presentPopover() {
    let popover = this.popoverCtrl.create(LanguagePopverPage);
    popover.present({
    });
  }
 
  getCurrentStorage() { 
    this.storage.get('lang').then((val) => {
      this.language = val == "en" ? 'English' : (val == 'ar' ? 'العربية' : 'English');
      return this.language;
    })

    this.storage.get('pushNotification').then((val) => {
      console.log(val);
      if (val == null) {
        this.pushNotification = false;
        // this.storage.set('pushNotification', false);
      } else {
        this.pushNotification = val;
      }
    })

    this.storage.get('newsLetter').then((val) => {
      if (val == null) {
        this.newsLetter = false;        
        this.storage.set('newsLetter', false);
      } else {
        this.newsLetter = val;
      }
    });
  }

  setLAnguageFromEvents() {
    this.events.subscribe('lang', (val) => {
      this.language = val == "en" ? 'English' : (val == 'ar' ? 'العربية' : 'English');
      return this.language;
    });
  }

  changePushNotification() {
    this.storage.set('pushNotification', this.pushNotification).then((data)=>{
      console.log(data);
    }).catch((err)=>{
      console.log(err);
    });
    console.log('done with ' + this.pushNotification);
  }

  changeNewsLetter() {
    this.storage.set('newsLetter', this.newsLetter).then((data) =>{
      console.log(data);
      console.log('done with ' + this.newsLetter);
    });
  }
}
