import { CashDeliveryPage } from '../cash-delivery/cash-delivery';
import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { AppSettingsProvider } from '../../providers/app-settings/app-settings';

@Component({
  selector: 'page-delivery-datails',
  templateUrl: 'delivery-datails.html',
})
export class DeliveryDatailsPage {



  productsCount: number;
  totalPrice: number;



  constructor(public navCtrl: NavController, public navParams: NavParams,
    public appSettingsProvider: AppSettingsProvider, public toastCtrl: ToastController) {

    this.productsCount = this.navParams.get("productCount");
    this.totalPrice = this.navParams.get("total");

  }


  // clear cart
  gotoPaymentMethodPage() {
    // let msg = this.getCurrentLang() == 'ar' ? 'تحت التطوير' : 'under development';
    // let toast = this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });
    // toast.present();
    this.navCtrl.push(CashDeliveryPage);
  }



  getCurrentLang() {
    return this.appSettingsProvider.Language;
  }



}
