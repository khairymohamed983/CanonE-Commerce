import { CreditCardPage } from '../credit-card/credit-card';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-cash-delivery',
  templateUrl: 'cash-delivery.html',
})
export class CashDeliveryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  confirmOrder() {
    this.navCtrl.push(CreditCardPage);
  }

}
